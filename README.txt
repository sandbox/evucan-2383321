Config Importer
===============

## SUMMARY

This module automates the config import process for various Drupal modules that
have a manual export/import process:  Views, Rules, Message, Flags, Panels and Panel pages.

For a full description of the module, visit the project page:
  http://drupal.org/project/config_importer

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/config_importer



## USEAGE

* When features is not being used on a project but you want to automate deployment.



## REQUIREMENTS

* Drupal version: 7.x-2.x.
* Folder: 	sites/all/imports
** Folder and file extension per config type:
		sites/all/imports/flags/*.inc 
		sites/all/imports/message_types/*.json
		sites/all/imports/rules/*.json
		sites/all/imports/views/*.inc 
		sites/all/imports/panel_variants/*.inc 
		sites/all/imports/pages/*.inc 

*** The '.inc' files are for exports that have php code 
*** The '.json' files are for exports that use json notation


## INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further information.



## HOW TO INSTALL

* Enable the module. 
* Go to role permisson page (admin/people/permissions). Ensure 
"Administer site configuration" for the role that has intended use.



## HOW TO USE

### EXPORT
* Copy the export code as per the manual export process of the given module.  
* Paste the code into a file into a directory for the given config type as 
per the 'Requirements' section


### IMPORTING

* Run the import process using the menu hook or from an update hook.  
* The parameters required by the import process are :
** Filename (without extension), or set to 'all' if you would like to import all config types
** Import type. Can be one of the following:
*** 'flags'
*** 'message_types'
*** 'rules'
*** 'views'
*** 'panel_variants'
*** 'pages'
*** 'all' - for all the import types



#### USING THE MENU HOOK

* Import 'all' the config for a given import type: 
	<SITE_URL>/config_importer/<IMPORT_TYPE>/all

* Import a specific file for a given import type: 
	<SITE_URL>/config_importer/<IMPORT_TYPE>/<FILENAME_WITHOUT_EXTENSION>

* Import 'all' the config for all import types: 
	<SITE_URL>/config_importer/all/all


#### FROM CODE 
* Import 'all' the config for a given import type: 
	config_importer_import('all', '<IMPORT_TYPE>');

* Import a specific file for a given import type: 
	config_importer_import('all', '<FILENAME_WITHOUT_EXTENSION>');

* Import 'all' the config for all import types: 
	config_importer_import('all', 'all');


#### EXAMPLES
* Importing an import file for a Flag using a menu hook
	<SITE_URL>/config_importer/flags/product_reviewed
* Importing an import file for a Flag from code
	config_importer_import('product_reviewed', 'flags');
* Importing an import file for a Rule using a menu hook
	<SITE_URL>/config_importer/rules/check_product
* Importing an import file for a Rule from code
	config_importer_import('check_product', 'rules');



### REPORTING
* See the Drupal recent reports (admin/reports/dblog) to view status of the import process.
* If any part of the process import process fails then FALSE is returned.

### OTHER INFORMATION
* Good practice:
** a. Import using update hooks as per the example
** b. Export/Import panel variants relating to only non core pages (core=node_view, user_view)
*** This is because:
	- currently there is a bug that creates duplicate panel_variants. 
	- You can not export core pages so we have to import config using panel variants

* Note:
** When importing Panel variants are 

### TODO
* Drush support
* Better error reporting
* Admin console


## TROUBLESHOOTING

See online issue at http://drupal.org/project/issues/config_importer.

